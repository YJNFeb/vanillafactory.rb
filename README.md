
# VanillaFactory

A Ruby script that instantiates a clone of Vanilla to get started on a new iOS project.

### Prerequisites

```
Xcode 8.0+
Swift 4.0+ 
```

You also need to install CocoaPods and xcodeproj.
```
$ sudo gem install cocoapods
$ sudo gem install xcodeproj
```

### Usage

Running this command will generate a clone of iOS project template Vanilla using the given project name (${PROJECT_NAME}):

```
$ pod lib create ${PROJECT_NAME} --template-url=https://YJNFeb@bitbucket.org/YJNFeb/vanillafactory.rb.git
```

You will also be prompted for a class prefix. If everything goes smoothly Xcode will automatically open the new cloned project so you can start coding right away.

## Acknowledgments

* CocoaPods and xcodeproj
* Yeojinet
* Jacky Lao
