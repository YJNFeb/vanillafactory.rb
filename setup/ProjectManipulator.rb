require 'xcodeproj'

module Pod

  class ProjectManipulator
    attr_reader :configurator, :xcodeproj_path, :platform, :remove_demo_target, :string_replacements, :prefix

    def self.perform(options)
      new(options).perform
    end

    def initialize(options)
      @xcodeproj_path = options.fetch(:xcodeproj_path)
      @configurator = options.fetch(:configurator)
      @platform = options.fetch(:platform)
      @remove_demo_target = options.fetch(:remove_demo_project)
      @prefix = options.fetch(:prefix)
    end

    def run
      @string_replacements = {
        #"PROJECT_OWNER" => @configurator.user_name,
        "Vanilla" => @configurator.pod_name,
        "vanilla" => @configurator.pod_name,
        "XXXX" => @prefix
      }
      replace_internal_project_settings

      @project = Xcodeproj::Project.open(@xcodeproj_path)
      @project.save

      rename_files
      rename_project_folder
    end

    def project_folder
      File.dirname @xcodeproj_path
    end

    def rename_files
      # rename xcproject
      File.rename(project_folder + "/Vanilla.xcodeproj", project_folder + "/" +  @configurator.pod_name + ".xcodeproj")

      unless @remove_demo_target
        [
          "XXXXAppDelegate.h", 
          "XXXXAppDelegate.m", 
          "XXXXTutorialPageDetails.swift"
        ].each do |file|
          before = project_folder + "/Vanilla/" + file
          next unless File.exists? before

          after = project_folder + "/Vanilla/" + file.gsub("XXXX", prefix)
          File.rename before, after
        end

        [
          "XXXXTutorialViewController.h", 
          "XXXXTutorialViewController.m", 
          "XXXXTutorialPageViewController.h", 
          "XXXXTutorialPageViewController.m", 
          "XXXXTutorialContentViewController.h", 
          "XXXXTutorialContentViewController.m",
        ].each do |file|
          before = project_folder + "/Vanilla/Tutorial/" + file
          next unless File.exists? before

          after = project_folder + "/Vanilla/Tutorial/" + file.gsub("XXXX", prefix)
          File.rename before, after
        end

        ["XXXXTabBarViewController.swift"].each do |file|
          before = project_folder + "/Vanilla/TabBar/" + file
          next unless File.exists? before

          after = project_folder + "/Vanilla/TabBar/" + file.gsub("XXXX", prefix)
          File.rename before, after
        end

        [ 
          "XXXXHomeViewController.swift", 
          "XXXXMenuViewController.swift", 
          "XXXXMenuTableViewCell.swift"
        ].each do |file|
          before = project_folder + "/Vanilla/SideMenu/" + file
          next unless File.exists? before

          after = project_folder + "/Vanilla/SideMenu/" + file.gsub("XXXX", prefix)
          File.rename before, after
        end

        [
          "XXXXSettingsTableViewController.swift", 
          "XXXXAboutViewController.swift"
        ].each do |file|
          before = project_folder + "/Vanilla/Settings/" + file
          next unless File.exists? before

          after = project_folder + "/Vanilla/Settings/" + file.gsub("XXXX", prefix)
          File.rename before, after
        end

        ["XXXXSegmentedPagerViewController.swift"].each do |file|
          before = project_folder + "/Vanilla/SegmentedPager/" + file
          next unless File.exists? before

          after = project_folder + "/Vanilla/SegmentedPager/" + file.gsub("XXXX", prefix)
          File.rename before, after
        end

        [ 
          "XXXXInitialViewController.h", 
          "XXXXInitialViewController.m"
        ].each do |file|
          before = project_folder + "/Vanilla/Initial/" + file
          next unless File.exists? before

          after = project_folder + "/Vanilla/Initial/" + file.gsub("XXXX", prefix)
          File.rename before, after
        end

        # rename project related files
        ["Vanilla.entitlements", "Vanilla-Bridging-Header.h"].each do |file|
          before = project_folder + "/Vanilla/" + file
          next unless File.exists? before

          after = project_folder + "/Vanilla/" + file.gsub("Vanilla", @configurator.pod_name)
          File.rename before, after
        end
      end
    end

    def rename_project_folder
      if Dir.exist? project_folder + "/Vanilla"
        File.rename(project_folder + "/Vanilla", project_folder + "/" + @configurator.pod_name)
      end
    end

    def replace_internal_project_settings
      Dir.glob(project_folder + "/**/**/**/**").each do |name|
        next if Dir.exists? name
        text = File.read(name)

        for find, replace in @string_replacements
            text = text.gsub(find, replace)
        end

        File.open(name, "w") { |file| file.puts text }
      end
    end
  end
end
