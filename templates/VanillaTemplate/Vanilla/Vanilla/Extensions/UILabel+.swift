//
//  UILabel+.swift
//

import Foundation
import UIKit

extension UILabel {
    @objc var fontSize: CGFloat {
        get {
            return font.pointSize
        }
        set {
            font = UIFont(name: font.fontName, size: newValue)
        }
    }
}
