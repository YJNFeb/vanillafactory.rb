//
//  UIScreen+.swift
//

import Foundation
import UIKit

extension UIScreen {
    @objc static let width  = UIScreen.main.bounds.width
    @objc static let height = UIScreen.main.bounds.height
    @objc static let size   = UIScreen.main.bounds.size
}
