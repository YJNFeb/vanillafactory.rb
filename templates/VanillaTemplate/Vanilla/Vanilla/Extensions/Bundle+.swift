//
//  Bundle+.swift
//

import Foundation
import UIKit

extension Bundle {
    @objc var name: String {
        return localizedInfoDictionary?["CFBundleName"] as! String
    }
    
    @objc var displayName: String {
        return localizedInfoDictionary?["CFBundleDisplayName"] as! String
    }
    
    @objc var identifier: String {
        return bundleIdentifier!
    }
    
    @objc var shortVersionString: String {
        return infoDictionary?["CFBundleShortVersionString"] as! String
    }
    
    @objc var version: String {
        return infoDictionary?["CFBundleVersion"] as! String
    }
    
    @objc var icon: UIImage? {
        guard let icons = localizedInfoDictionary?["CFBundleIcons"] as? [String: Any] else {
            return nil
        }
        guard let primaryIcon = icons["CFBundlePrimaryIcon"] as? [String: Any] else {
            return nil
        }
        guard let iconFiles = primaryIcon["CFBundleIconFiles"] as? [String] else {
            return nil
        }
        guard let iconName = iconFiles.last else {
            return nil
        }
        return UIImage(named: iconName)
    }
}
