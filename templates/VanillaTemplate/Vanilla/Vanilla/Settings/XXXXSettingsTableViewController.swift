//
//  XXXXSettingsViewController.swift
//

import UIKit
import UserNotifications
import StoreKit

final class XXXXSettingsTableViewController: UITableViewController {
    @IBOutlet private weak var notificationsLabel: UILabel! {
        didSet {
            notificationsLabel.text = "NOTIFICATIONS"
            notificationsLabel.textColor = .black
            notificationsLabel.font = UIFont.generateDefaultFont(withType: 4, size: 20)
        }
    }
    @IBOutlet private weak var notificationsSwitch: UISwitch!
    @IBOutlet private weak var aboutLabel: UILabel! {
        didSet {
            aboutLabel.text = "ABOUT"
            aboutLabel.textColor = .black
            aboutLabel.font = UIFont.generateDefaultFont(withType: 4, size: 20)
        }
    }
    @IBOutlet private weak var appDisplayNameLabel: UILabel! {
        didSet {
            appDisplayNameLabel.text = Bundle.main.displayName
            appDisplayNameLabel.textColor = .lightGray
            appDisplayNameLabel.font = UIFont.generateDefaultFont(withType: 4, size: 20)
        }
    }
    @IBOutlet private weak var versionLabel: UILabel! {
        didSet {
            versionLabel.text = "VERSION"
            versionLabel.textColor = .black
            versionLabel.font = UIFont.generateDefaultFont(withType: 4, size: 20)
        }
    }
    @IBOutlet private weak var versionNumberLabel: UILabel! {
        didSet {
            versionNumberLabel.text = Bundle.main.shortVersionString
            versionNumberLabel.textColor = .lightGray
            versionNumberLabel.font = UIFont.generateDefaultFont(withType: 4, size: 20)
        }
    }
    @IBOutlet private weak var shareLabel: UILabel! {
        didSet {
            shareLabel.text = "SHARE"
            shareLabel.textColor = .black
            shareLabel.font = UIFont.generateDefaultFont(withType: 4, size: 20)
        }
    }
    @IBOutlet private weak var rateTheAppLabel: UILabel! {
        didSet {
            rateTheAppLabel.text = "RATE: \(Bundle.main.displayName.uppercased())"
            rateTheAppLabel.textColor = .black
            rateTheAppLabel.font = UIFont.generateDefaultFont(withType: 4, size: 20)
        }
    }
    @IBOutlet private weak var fiveStarsLabel: UILabel! {
        didSet {
            fiveStarsLabel.text = "⭐️⭐️⭐️⭐️⭐️"
            fiveStarsLabel.textColor = .black
            fiveStarsLabel.font = UIFont.generateDefaultFont(withType: 4, size: 20)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        title = "⚙️ SETTINGS"
        tabBarItem = UITabBarItem(title: "Settings", image: #imageLiteral(resourceName: "IconSettings"), tag: 4)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.backgroundColor = .white
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [.font: UIFont.generateDefaultFont(withType: 4, size: 22)]
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.largeTitleTextAttributes = [.font: UIFont.generateDefaultFont(withType: 4, size: 40)]
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNotificationsSwitch()
    }
    
    private func setupNotificationsSwitch() {
        guard UIApplication.shared.isRegisteredForRemoteNotifications else {
            notificationsSwitch.setOn(false, animated: true)
            return
        }
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
                DispatchQueue.main.async {
                    self.notificationsSwitch.setOn(notificationSettings.authorizationStatus == .authorized, animated: true)
                }
            }
        } else {
            notificationsSwitch.setOn(UIApplication.shared.isRegisteredForRemoteNotifications, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                showAbout()
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                showSharePrompt()
            } else if indexPath.row == 1 {
                showRatePrompt()
            }
        }
    }
    
    private func showAbout() {
        let aboutViewController = self.storyboard?.instantiateViewController(withIdentifier: "XXXXAboutViewController") as! XXXXAboutViewController
        navigationController?.pushViewController(aboutViewController, animated: true)
    }
    
    private func showSharePrompt() {
        var activityItems: [Any] = [AppConstants.appShareText, AppConstants.appStoreURL]
        if let iconImage = Bundle.main.icon {
            activityItems.append(iconImage)
        }
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.airDrop, .addToReadingList, .assignToContact, .openInIBooks]
        if #available(iOS 11.0, *) {
            activityViewController.excludedActivityTypes?.append(.markupAsPDF)
        }
        present(activityViewController, animated: true, completion: nil)
    }
    
    private func showRatePrompt() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else if #available(iOS 10.0, *) {
            UIApplication.shared.open(AppConstants.appStoreURL, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(AppConstants.appStoreURL)
        }
    }
    
    @IBAction private func toggleNotificationSetting(_ sender: UISwitch) {
        print("$ toggleNotificationSetting: => { isOn: \(sender.isOn) }")
        print("$ userDefaults => { didUnregisterForRemoteNotificationsKey: \(!sender.isOn) }")
        UserDefaults.standard.set(!sender.isOn, forKey: UserDefaults.didUnregisterForRemoteNotificationsKey)
        if sender.isOn {
            setupRemoteNotifications()
        } else {
            UIApplication.shared.unregisterForRemoteNotifications()
        }
    }
    
    private func setupRemoteNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (didGrant, error) in
                guard error == nil else {
                    print("$ userNotificationAuthorizationError => \(error!.localizedDescription)")
                    return
                }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
}



