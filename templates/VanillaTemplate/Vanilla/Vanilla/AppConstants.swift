//
//  AppConstants.swift
//

import Foundation
import UIKit

final class AppConstants: NSObject {
    private static let appStoreURLString = ""
    static let appStoreURL: URL = {
        guard let url = URL(string: AppConstants.appStoreURLString) else {
            fatalError("$ appStoreURL => 'Please set appStoreURLString in AppConstants.'")
        }
        return url
    }()
    
    static let appShareText = "Check out this awesome app: \(Bundle.main.displayName)!"
    static let appDescriptionText = "Thank you for downloading \(Bundle.main.displayName)."
    
    private static let _tutorialPageDetails = [
        XXXXTutorialPageDetails(image: nil, title: "First Page", text: "This is the first page."),
        XXXXTutorialPageDetails(image: nil, title: "Second Page", text: "This is the second page."),
        XXXXTutorialPageDetails(image: nil, title: "Third Page", text: "This is the third page.")
    ]
    
    @objc static var tutorialPageDetails: [XXXXTutorialPageDetails] {
        guard !_tutorialPageDetails.isEmpty else {
            fatalError("$ tutorialPagesError => 'Please set tutorial pages content.'")
        }
        return _tutorialPageDetails
    }
    
    static let _mainViewControllers = [
        UIStoryboard.examples.instantiateViewController(withIdentifier: "ZeroViewController"),
        UIStoryboard.examples.instantiateViewController(withIdentifier: "OneViewController"),
        UIStoryboard.examples.instantiateViewController(withIdentifier: "TwoViewController"),
        UIStoryboard.examples.instantiateViewController(withIdentifier: "ThreeViewController"),
        UIStoryboard.settings.instantiateViewController(withIdentifier: "XXXXSettingsNavigationController"),//"SettingsTableViewController"),
    ]
    
    static let _segmentedPagerTitles = [
        MainViewController.zero.segmentedPagerTitle,
        MainViewController.one.segmentedPagerTitle,
        MainViewController.two.segmentedPagerTitle,
        MainViewController.three.segmentedPagerTitle,
        MainViewController.four.segmentedPagerTitle
    ]
    
    static let _viewControllerInfo: [MainViewController] = [.zero, .one, .two, .three, .four]
    enum MainViewController {
        case zero
        case one
        case two
        case three
        case four
        
        var tabBarItemTitle: String {
            switch self {
            case .zero:
                return "Zero"
            case .one:
                return "One"
            case .two:
                return "Two"
            case .three:
                return "Three"
            case .four:
                return "Settings"
            }
        }
        
        var segmentedPagerTitle: String {
            switch self {
            case .zero:
                return "0"
            case .one:
                return "1"
            case .two:
                return "2"
            case .three:
                return "3"
            case .four:
                return "4"
            }
        }
    }
}

extension UserDefaults {
    @objc static let didUnregisterForRemoteNotificationsKey = "didUnregisterForRemoteNotificationsKey"
    @objc static let didFinishTutorialKey                   = "didFinishTutorialKey"
}

extension UIStoryboard {
    static let examples         = UIStoryboard(name: "Examples", bundle: nil)
    static let initial          = UIStoryboard(name: "Initial", bundle: nil)
    static let segmentedPager   = UIStoryboard(name: "SegmentedPager", bundle: nil)
    static let settings         = UIStoryboard(name: "Settings", bundle: nil)
    static let sideMenu         = UIStoryboard(name: "SideMenu", bundle: nil)
    static let tabBar           = UIStoryboard(name: "TabBar", bundle: nil)
    static let tutorial         = UIStoryboard(name: "Tutorial", bundle: nil)
}

extension UIFont {
    private static let defaultFontType = "HelveticaNeue-Medium"
    private static let _presetFontTypes = [
        "Gotham-Black",
        "Gotham-Bold",
        "Gotham-Medium",
        "Gotham-Light",
        "HelveticaNeue-CondensedBlack",
        "HelveticaNeue-CondensedBold",
    ]
    static var presetFontTypes: [String] {
        guard !_presetFontTypes.isEmpty else {
            print("$ fontTypeError => 'No preset font types set. Will use \(defaultFontType) as default font instead.'")
            return [UIFont.defaultFontType]
        }
        return _presetFontTypes
    }
    
    @objc static func generateDefaultFont(withType type: Int, size: CGFloat) -> UIFont {
        guard type >= 0 && type < _presetFontTypes.count else {
            print("$ fontTypeError => 'Unable to retrieve specified font type. Will use \(defaultFontType) as default font instead.'")
            return UIFont(name: defaultFontType, size: size)!
        }
        guard let font = UIFont(name: _presetFontTypes[type], size: size) else {
            print("$ fontTypeError => 'Unable to retrieve specified font type. Will use \(defaultFontType) as default font instead.'")
            return UIFont(name: defaultFontType, size: size)!
        }
        return font
    }
}



