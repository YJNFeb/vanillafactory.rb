//
//  XXXXTutorialViewController.m
//

#import "XXXXTutorialViewController.h"
#import "Vanilla-Swift.h"

@interface XXXXTutorialViewController ()

@property (nonatomic, weak) IBOutlet UIButton *getStartedButton;

@end

@implementation XXXXTutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.getStartedButton.titleLabel.font = [UIFont generateDefaultFontWithType:4 size:20];
}

- (IBAction)getStarted:(id)sender {
    NSLog(@"$ getStarted:");
    
    [NSUserDefaults.standardUserDefaults setBool:YES forKey:NSUserDefaults.didFinishTutorialKey];
    NSLog(@"$ userDefaults => { %@: true }", NSUserDefaults.didFinishTutorialKey);
    
    [self performSegueWithIdentifier:@"PresentTabBar" sender:sender];
//    [self performSegueWithIdentifier:@"PresentSegmentedPager" sender:sender];
//    [self performSegueWithIdentifier:@"PresentMenu" sender:sender];
}

@end
