//
//  XXXXHomeViewController.swift
//

import UIKit
import SideMenu

final class XXXXHomeViewController: UIViewController {
    @IBOutlet private weak var menuBarButtonItem: UIBarButtonItem! {
        didSet {
            let attributes = [NSAttributedStringKey.font: UIFont.generateDefaultFont(withType: 4, size: 18)]
            menuBarButtonItem.setTitleTextAttributes(attributes, for: .normal)
            menuBarButtonItem.title = "MENU"
        }
    }
    @IBOutlet private weak var scrollView: UIScrollView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        extendedLayoutIncludesOpaqueBars = true
        edgesForExtendedLayout = .top
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.backgroundColor = .white
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.titleTextAttributes = [.font: UIFont.generateDefaultFont(withType: 4, size: 22)]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).setTitleTextAttributes([.font: UIFont.generateDefaultFont(withType: 4, size: 22)], for: .normal)
        
        let barButtonItem = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        navigationItem.backBarButtonItem = barButtonItem
        navigationController?.navigationBar.backItem?.backBarButtonItem = barButtonItem
        navigationController?.navigationBar.backItem?.backBarButtonItem?.title = ""
        
        let menuViewController = storyboard?.instantiateViewController(withIdentifier: "XXXXMenuViewController") as! XXXXMenuViewController
        menuViewController.delegate = self
        let sideMenuNavigationController = UISideMenuNavigationController(rootViewController: menuViewController)
        sideMenuNavigationController.navigationBar.shadowImage = UIImage()
        sideMenuNavigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        sideMenuNavigationController.navigationBar.barTintColor = .clear
        sideMenuNavigationController.navigationBar.backgroundColor = .clear
        if #available(iOS 11.0, *) {
            sideMenuNavigationController.navigationBar.prefersLargeTitles = true
        }
        
        SideMenuManager.default.menuLeftNavigationController = sideMenuNavigationController
        SideMenuManager.default.menuRightNavigationController = nil
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: view, forMenu: .left)
        SideMenuManager.default.menuAddPanGestureToPresent(toView: navigationController!.navigationBar)
        SideMenuManager.default.menuAddPanGestureToPresent(toView: view)
        SideMenuManager.default.menuDismissOnPush = true
        SideMenuManager.default.menuAlwaysAnimate = true
        SideMenuManager.default.menuShadowRadius = 10
        SideMenuManager.default.menuShadowColor = .lightGray
        SideMenuManager.default.menuFadeStatusBar = false
//        SideMenuManager.default.menuAnimationUsingSpringWithDamping = 0.75
//        SideMenuManager.default.menuAnimationInitialSpringVelocity = 2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.largeTitleTextAttributes = [.font: UIFont.generateDefaultFont(withType: 4, size: 40)]
        }
    }
    
    @IBAction private func showLeftSideMenu(_ sender: UIBarButtonItem) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
}

extension XXXXHomeViewController: XXXXMenuViewControllerDelegate {
    func menuViewController(_ menuViewController: XXXXMenuViewController, didSelectRowAt indexPath: IndexPath) {
        let viewController = AppConstants._mainViewControllers[indexPath.row]
        if #available(iOS 11.0, *) {
            if indexPath.row == 4 {
                navigationController?.navigationBar.prefersLargeTitles = true
            } else {
                navigationController?.navigationBar.prefersLargeTitles = false
            }
        }
//        navigationController?.pushViewController(viewController, animated: true)
        navigationController?.present(viewController, animated: true, completion: nil)
    }
}



