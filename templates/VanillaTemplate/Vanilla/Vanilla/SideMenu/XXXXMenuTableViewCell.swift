//
//  XXXXMenuTableViewCell.swift
//

import UIKit

final class XXXXMenuTableViewCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        textLabel?.font = UIFont.generateDefaultFont(withType: 4, size: 32)
        imageView?.tintColor = .black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
