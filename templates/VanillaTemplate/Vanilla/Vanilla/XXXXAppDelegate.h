//
//  XXXXAppDelegate.h
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

@interface XXXXAppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

