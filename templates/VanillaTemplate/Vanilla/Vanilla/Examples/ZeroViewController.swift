//
//  ZeroViewController.swift
//

import UIKit

class ZeroViewController: UIViewController {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
