//
//  FourViewController.swift
//

import UIKit

class FourViewController: UIViewController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 4)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
