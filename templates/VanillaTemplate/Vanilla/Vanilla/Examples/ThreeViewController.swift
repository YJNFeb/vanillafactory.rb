//
//  ThreeViewController.swift
//

import UIKit

class ThreeViewController: UIViewController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 3)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
