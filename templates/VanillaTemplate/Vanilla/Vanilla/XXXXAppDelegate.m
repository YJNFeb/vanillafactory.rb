//
//  XXXXAppDelegate.m
//

#import "XXXXAppDelegate.h"
#import "Vanilla-Swift.h"

@interface XXXXAppDelegate ()

@end

@implementation XXXXAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self handleRemoteNotification:launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]];
    self.window.backgroundColor = UIColor.whiteColor;
    return YES;
}

#pragma mark -

- (void)handleRemoteNotification:(NSDictionary *)userInfo {
    if (userInfo == nil) {
        return;
    }
    NSLog(@"$ userInfo => %@", userInfo);
    
    __block UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    alertWindow.rootViewController = [[UIViewController alloc] init];
    alertWindow.windowLevel = UIWindowLevelAlert;
    
    NSString *message = userInfo[@"aps"][@"alert"];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSBundle.mainBundle.displayName message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        alertWindow.hidden = YES;
        alertWindow = nil;
    }]];
    [alertWindow makeKeyAndVisible];
    [alertWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    NSLog(@"$ application:didRegisterUserNotificationSettings:");
    [NSNotificationCenter.defaultCenter postNotificationName:@"application:didRegisterUserNotificationSettings:" object:nil];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"<>"];
    NSString *trimmedDeviceTokenDescription = [deviceToken.description stringByTrimmingCharactersInSet:characterSet];
    NSString *deviceTokenString = [trimmedDeviceTokenDescription stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"$ deviceToken => %@", deviceTokenString);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"$ didFailToRegisterForRemoteNotificationsWithError => %@", error.localizedDescription);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [self handleRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [self handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

#pragma mark - UNUserNotificationCenterDelegate

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler API_AVAILABLE(ios(10.0)) {
    
    NSDictionary *userInfo = notification.request.content.userInfo;
    [self handleRemoteNotification:userInfo];
    
    UNNotificationPresentationOptions options = UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound;
    completionHandler(options);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler API_AVAILABLE(ios(10.0)) {
    
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    [self handleRemoteNotification:userInfo];
    
    completionHandler();
}

@end



