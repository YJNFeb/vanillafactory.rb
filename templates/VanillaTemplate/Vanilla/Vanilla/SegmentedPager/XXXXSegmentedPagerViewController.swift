//
//  XXXXSegmentedPagerViewController.swift
//

import UIKit
import MXSegmentedPager

final class XXXXSegmentedPagerViewController: MXSegmentedPagerController {

    @IBOutlet private weak var headerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentedPager.backgroundColor = .white
        segmentedPager.parallaxHeader.view = headerView
        segmentedPager.parallaxHeader.mode = .fill
        segmentedPager.parallaxHeader.height = 128
        segmentedPager.parallaxHeader.minimumHeight = 64
        
        segmentedPager.segmentedControl.selectionIndicatorLocation = .down
        segmentedPager.segmentedControl.backgroundColor = .white
        segmentedPager.segmentedControl.selectionIndicatorColor = .black
        segmentedPager.segmentedControl.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: UIColor.lightGray,
            NSAttributedStringKey.font: UIFont.generateDefaultFont(withType: 0, size: 20)
        ]
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        segmentedPager.segmentedControl.selectionStyle = .fullWidthStripe
        segmentedPager.segmentedControl.selectionIndicatorHeight = 5;
    }
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return AppConstants._mainViewControllers.count
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return AppConstants._segmentedPagerTitles[index]
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        return AppConstants._mainViewControllers[index]
    }
}
