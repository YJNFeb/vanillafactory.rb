//
//  main.m
//

#import <UIKit/UIKit.h>
#import "XXXXAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XXXXAppDelegate class]));
    }
}
