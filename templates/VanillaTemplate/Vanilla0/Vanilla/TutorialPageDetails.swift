//
//  TutorialPage.swift
//

import Foundation
import UIKit

@objc final class TutorialPageDetails: NSObject {
    @objc var image: UIImage?
    @objc var title: String?
    @objc var text: String?
    
    init(image: UIImage? = nil, title: String = "", text: String = "") {
        self.image = image
        self.title = title
        self.text = text
    }
}
