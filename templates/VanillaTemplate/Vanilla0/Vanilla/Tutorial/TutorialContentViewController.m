//
//  TutorialContentViewController.m
//

#import "TutorialContentViewController.h"
#import "Vanilla-Swift.h"

@interface TutorialContentViewController ()

@property (nonatomic, weak) IBOutlet UIImageView *pageImageView;
@property (nonatomic, weak) IBOutlet UILabel *pageTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *pageTextLabel;

@end

@implementation TutorialContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageImageView.image = self.tutorialPageDetails.image;
    
    self.pageTitleLabel.font = [UIFont generateDefaultFontWithType:4 size:24];
    self.pageTitleLabel.text = self.tutorialPageDetails.title.uppercaseString;
    
    self.pageTextLabel.font = [UIFont generateDefaultFontWithType:3 size:20];
    self.pageTextLabel.text = self.tutorialPageDetails.text;
}

@end
