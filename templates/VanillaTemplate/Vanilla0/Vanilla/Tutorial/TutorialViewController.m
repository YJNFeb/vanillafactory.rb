//
//  TutorialViewController.m
//

#import "TutorialViewController.h"
#import "Vanilla-Swift.h"

@interface TutorialViewController ()

@property (nonatomic, weak) IBOutlet UIButton *getStartedButton;

@end

@implementation TutorialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.getStartedButton.titleLabel.font = [UIFont generateDefaultFontWithType:4 size:20];
}

- (IBAction)getStarted:(id)sender {
    NSLog(@"$ getStarted:");
    
    [NSUserDefaults.standardUserDefaults setBool:YES forKey:NSUserDefaults.didFinishTutorialKey];
    NSLog(@"$ userDefaults => { %@: true }", NSUserDefaults.didFinishTutorialKey);
    
    [self performSegueWithIdentifier:@"PresentTabBar" sender:sender];
//    [self performSegueWithIdentifier:@"PresentSegmentedPager" sender:sender];
//    [self performSegueWithIdentifier:@"PresentMenu" sender:sender];
}

@end
