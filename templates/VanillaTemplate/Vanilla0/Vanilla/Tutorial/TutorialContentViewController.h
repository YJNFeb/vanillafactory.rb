//
//  TutorialContentViewController.h
//

#import <UIKit/UIKit.h>

@class TutorialPageDetails;

@interface TutorialContentViewController : UIViewController

@property (nonatomic, strong) TutorialPageDetails *tutorialPageDetails;

@end
