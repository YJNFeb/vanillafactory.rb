//
//  TutorialPageViewController.m
//

#import "TutorialPageViewController.h"
#import "Vanilla-Swift.h"
#import "TutorialContentViewController.h"

@interface TutorialPageViewController () <UIPageViewControllerDataSource>

@property (nonatomic, strong) NSArray<UIViewController *> *contentViewControllers;

@end

@implementation TutorialPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupPageControlAppearance];
    self.dataSource = self;
    self.contentViewControllers = [self generateContentViewControllers];
    [self setViewControllers:@[self.contentViewControllers[0]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

- (void)setupPageControlAppearance {
    UIPageControl *pageControlAppearance = [UIPageControl appearanceWhenContainedInInstancesOfClasses:@[TutorialPageViewController.class]];
    [pageControlAppearance setPageIndicatorTintColor:UIColor.lightGrayColor];
    [pageControlAppearance setCurrentPageIndicatorTintColor:UIColor.blackColor];
    [pageControlAppearance setBackgroundColor:UIColor.clearColor];
}

- (NSArray<UIViewController *> *)generateContentViewControllers {
    NSMutableArray *viewControllers = [NSMutableArray array];
    for (TutorialPageDetails *tutorialPageDetails in AppConstants.tutorialPageDetails) {
        TutorialContentViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialContentViewController"];
        viewController.tutorialPageDetails = tutorialPageDetails;
        [viewControllers addObject:viewController];
    }
    return viewControllers;
}

#pragma mark - UIPageViewControllerDataSource

- (nullable UIViewController *)pageViewController:(nonnull UIPageViewController *)pageViewController viewControllerBeforeViewController:(nonnull UIViewController *)viewController {
    NSInteger pageIndex = [self.contentViewControllers indexOfObject:viewController];
    if (pageIndex == NSNotFound) {
        return nil;
    }
    if (pageIndex == 0) {
        return nil;
    }
    NSInteger previousPageIndex = pageIndex - 1;
    return self.contentViewControllers[previousPageIndex];
}

- (nullable UIViewController *)pageViewController:(nonnull UIPageViewController *)pageViewController viewControllerAfterViewController:(nonnull UIViewController *)viewController {
    NSInteger pageIndex = [self.contentViewControllers indexOfObject:viewController];
    if (pageIndex == NSNotFound) {
        return nil;
    }
    NSInteger nextPageIndex = pageIndex + 1;
    if (nextPageIndex == self.contentViewControllers.count) {
        return nil;
    }
    return self.contentViewControllers[nextPageIndex];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return self.contentViewControllers.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

@end
