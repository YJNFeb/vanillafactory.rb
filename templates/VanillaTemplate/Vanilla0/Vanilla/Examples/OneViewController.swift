//
//  OneViewController.swift
//

import UIKit

class OneViewController: UIViewController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 1)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
