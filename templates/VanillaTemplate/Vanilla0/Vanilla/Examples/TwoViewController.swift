//
//  TwoViewController.swift
//

import UIKit

class TwoViewController: UIViewController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 2)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
