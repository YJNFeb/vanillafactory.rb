//
//  MenuViewController.swift
//

import UIKit
import Haptica

protocol MenuViewControllerDelegate: AnyObject {
    func menuViewController(_ menuViewController: MenuViewController, didSelectRowAt indexPath: IndexPath)
}

final class MenuViewController: UIViewController {
    weak var delegate: MenuViewControllerDelegate?
    
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppConstants._mainViewControllers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MenuTableViewCell
        cell.textLabel?.text = AppConstants._viewControllerInfo[indexPath.row].tabBarItemTitle.uppercased()
        cell.imageView?.image = UIImage(named: "Icon")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Haptic.impact(.light).generate()
        tableView.deselectRow(at: indexPath, animated: true)
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: {
                self.delegate?.menuViewController(self, didSelectRowAt: indexPath)
            })
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
