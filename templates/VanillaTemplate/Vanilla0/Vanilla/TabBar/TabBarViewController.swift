//
//  TabBarViewController.swift
//

import UIKit

final class TabBarViewController: UITabBarController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).setTitleTextAttributes([.font: UIFont.generateDefaultFont(withType: 4, size: 22)], for: .normal)
        viewControllers = AppConstants._mainViewControllers
    }
}
