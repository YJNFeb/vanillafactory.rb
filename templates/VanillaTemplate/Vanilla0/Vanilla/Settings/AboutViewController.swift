//
//  AboutViewController.swift
//

import UIKit

final class AboutViewController: UIViewController {
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var appIconImageView: UIImageView! {
        didSet {
            appIconImageView.image = Bundle.main.icon
            appIconImageView.backgroundColor = UIColor.groupTableViewBackground
        }
    }
    @IBOutlet private weak var appDisplayNameLabel: UILabel! {
        didSet {
            appDisplayNameLabel.text = Bundle.main.displayName.uppercased()
            appDisplayNameLabel.textColor = .black
            appDisplayNameLabel.font = UIFont.generateDefaultFont(withType: 4, size: 28)
        }
    }
    @IBOutlet private weak var appVersionLabel: UILabel! {
        didSet {
            appVersionLabel.text = "Version-\(Bundle.main.shortVersionString) (Build-\(Bundle.main.version))"
            appVersionLabel.textColor = .lightGray
            appVersionLabel.font = UIFont.generateDefaultFont(withType: 5, size: 16)
        }
    }
    @IBOutlet private weak var appDescriptionLabel: UILabel! {
        didSet {
            appDescriptionLabel.text = AppConstants.appDescriptionText
            appDescriptionLabel.textColor = .lightGray
            appDescriptionLabel.font = UIFont.generateDefaultFont(withType: 5, size: 16)
        }
    }
    @IBOutlet private weak var copyrightLabel: UILabel! {
        didSet {
            copyrightLabel.text = "Copyright \(Date().year) \(Bundle.main.displayName). All rights reserved."
            copyrightLabel.textColor = .lightGray
            copyrightLabel.font = UIFont.generateDefaultFont(withType: 5, size: 16)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        title = "About".uppercased()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}



