//
//  InitialViewController.m
//

#import "InitialViewController.h"
#import "Vanilla-Swift.h"
#import <UserNotifications/UserNotifications.h>
#import "AppDelegate.h"

@interface InitialViewController () <UNUserNotificationCenterDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *appIconImageView;
@property (nonatomic, weak) IBOutlet UILabel *appDisplayNameLabel;

@end

@implementation InitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appIconImageView.image = NSBundle.mainBundle.icon;
    self.appDisplayNameLabel.font = [UIFont generateDefaultFontWithType:4 size:40];
    self.appDisplayNameLabel.text = NSBundle.mainBundle.displayName.uppercaseString;
    
    if (@available(iOS 10.0, *)) {
        
    } else {
        [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(showNextScreen) name:@"application:didRegisterUserNotificationSettings:" object:nil];
    }
    
    if ([NSUserDefaults.standardUserDefaults boolForKey:NSUserDefaults.didUnregisterForRemoteNotificationsKey]) {
        [self showNextScreen];
    } else {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self setupRemoteNotificationsWithCompletion:^{
                [self showNextScreen];
            }];
        });
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [NSNotificationCenter.defaultCenter removeObserver:self name:@"application:didRegisterUserNotificationSettings:" object:nil];
}

- (void)showNextScreen {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([NSUserDefaults.standardUserDefaults boolForKey:NSUserDefaults.didFinishTutorialKey]) {
//            [self performSegueWithIdentifier:@"PresentSideMenu" sender:nil];
            [self performSegueWithIdentifier:@"PresentTabBar" sender:nil];
//            [self performSegueWithIdentifier:@"PresentSegmentedPager" sender:nil];
        } else {
            [self performSegueWithIdentifier:@"PresentTutorial" sender:nil];
        }
    });
}

- (void)setupRemoteNotificationsWithCompletion:(void (^)(void))completionHandler {
    if (@available(iOS 10.0, *)) {
        UNUserNotificationCenter *notificationCenter = UNUserNotificationCenter.currentNotificationCenter;
        notificationCenter.delegate = (AppDelegate *)UIApplication.sharedApplication.delegate;
        UNAuthorizationOptions options = UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge;
        [notificationCenter requestAuthorizationWithOptions:options completionHandler:^(BOOL granted, NSError * _Nullable error) {
            completionHandler();
            if (error != nil) {
                NSLog(@"$ userNotificationAuthorizationError => %@", error.localizedDescription);
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self registerRemoteNotifications];
            });
        }];
    } else {
        [self registerRemoteNotifications];
    }
}

- (void)registerRemoteNotifications {
    UIUserNotificationType notificationType = UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
    UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:notificationType categories:nil];
    [UIApplication.sharedApplication registerUserNotificationSettings:notificationSettings];
    [UIApplication.sharedApplication registerForRemoteNotifications];
}

@end
